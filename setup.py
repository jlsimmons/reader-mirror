try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Reader',
    'author': 'J',
    'url': 'idk.',
    'download_url': 'idk.',
    'author_email': 'jls',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['READER'],
    'scripts': [],
    'name': 'reader'
}

setup(**config)